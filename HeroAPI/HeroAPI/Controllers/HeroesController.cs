﻿using HeroAPI.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace HeroAPI.Controllers
{
    public class HeroesController : ApiController
    {
        public IEnumerable<DOTAHero> Get()
        {
            HeroManager HM = new HeroManager();
            return HM.GetAll;
        }

        // GET api/values/7
        public DOTAHero Get(int id)
        {
            HeroManager HM = new HeroManager();
            return HM.GetHeroByID(id);
        }
    }
}